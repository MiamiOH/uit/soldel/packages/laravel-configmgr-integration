<?php

namespace Test;

use MiamiOH\ConfigManager\ConfigurationItem;
use MiamiOH\ConfigManager\DataParser;

class DataParserTest extends TestCase
{
    public function testCreatesCollectionOfItemsFromDataArray(): void
    {
        $itemData = $this->makeItemData('test-item', ['config_value' => 'test value']);

        $parser = new DataParser();

        $items = $parser->categoryFromData($itemData);

        $this->assertCount(1, $items);
        $this->assertTrue($items->has('test-item'));

        /** @var ConfigurationItem $item */
        $item = $items->get('test-item');

        $this->assertEquals('test value', $item->value());
    }
}
