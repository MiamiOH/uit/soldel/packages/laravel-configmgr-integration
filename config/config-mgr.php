<?php

return [
    'default-application' => env('CONFIGMGR_DEFAULT_APPLICATION', ''),
    'default-category' => env('CONFIGMGR_DEFAULT_CATEGORY', ''),

    'cache-seconds' => env('CONFIGMGR_CACHE_SECONDS', 3600),
];
