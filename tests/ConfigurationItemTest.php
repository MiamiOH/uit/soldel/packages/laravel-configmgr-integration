<?php

namespace Test;

use Carbon\Carbon;
use MiamiOH\ConfigManager\ConfigurationItem;
use MiamiOH\ConfigManager\Exceptions\ParseValueException;

class ConfigurationItemTest extends TestCase
{
    public function testReturnsName(): void
    {
        $name = $this->faker->word();

        $item = $this->makeItem($name, ['name' => $name]);

        $this->assertEquals($name, $item->name());
    }

    public function testReturnsApplication(): void
    {
        $name = $this->faker->word();

        $item = $this->makeItem('test-item', ['config_application' => $name]);

        $this->assertEquals($name, $item->application());
    }

    public function testReturnsCategory(): void
    {
        $name = $this->faker->word();

        $item = $this->makeItem('test-item', ['config_category' => $name]);

        $this->assertEquals($name, $item->category());
    }

    public function testReturnsDescription(): void
    {
        $description = $this->faker->word();

        $item = $this->makeItem('test-item', ['config_desc' => $description]);

        $this->assertEquals($description, $item->description());
    }

    public function testReturnsDataStructure(): void
    {
        $dataStructure = 'scalar';

        $item = $this->makeItem('test-item', ['config_data_structure' => $dataStructure]);

        $this->assertEquals($dataStructure, $item->dataStructure());
    }

    public function testReturnsDataType(): void
    {
        $dataType = 'text';

        $item = $this->makeItem('test-item', ['config_data_type' => $dataType]);

        $this->assertEquals($dataType, $item->dataType());
    }

    public function testReturnsValue(): void
    {
        $value = $this->faker->word();

        $item = $this->makeItem('test-item', ['config_value' => $value]);

        $this->assertEquals($value, $item->value());
    }

    public function testReturnsValueWithReplacements(): void
    {
        $word = $this->faker->word();

        $item = $this->makeItem('test-item', ['config_value' => 'Hello {word}']);

        $this->assertEquals(sprintf('Hello %s', $word), $item->value(['word' => $word]));
    }

    public function testReturnsUpdatedAt(): void
    {
        $updatedAt = Carbon::now()->subMinutes(random_int(10, 1000));

        $item = $this->makeItem('test-item', ['activity_date' => $updatedAt]);

        $this->assertEquals($updatedAt, $item->updatedAt());
    }

    public function testCanBeConstructedFromDataArray(): void
    {
        $dataArray = $this->makeItemData('test-item', ['config_value' => 'test value']);

        $item = ConfigurationItem::fromDataArray('test-item', $dataArray['test-item']);

        $this->assertEquals('test value', $item->value());
    }

    public function testHandlesNullDescription(): void
    {
        $item = $this->makeItem('test-item', ['config_desc' => null]);

        $this->assertEquals('', $item->description());
    }

    public function testHandlesNullValue(): void
    {
        $item = $this->makeItem('test-item', ['config_value' => null]);

        $this->assertEquals('', $item->value());
    }

    /**
     * @dataProvider valueAsBoolChecks
     */
    public function testReturnsValueAsBool(?string $value, bool $expected): void
    {
        $item = $this->makeItem('test-item', ['config_value' => $value]);

        $this->assertEquals($expected, $item->valueAsBool());
    }

    public static function valueAsBoolChecks(): array
    {
        return [
            'null' => [null, false],
            'empty string' => ['', false],
            'FALSE' => ['FALSE', false],
            'false' => ['false', false],
            'F' => ['F', false],
            'No' => ['No', false],
            'N' => ['N', false],
            'TRUE' => ['TRUE', true],
            'true' => ['true', true],
            't' => ['t', true],
            'Yes' => ['Yes', true],
            'Y' => ['Y', true],
        ];
    }

    public function testReturnsValueAsDate(): void
    {
        $item = $this->makeItem('test-item', ['config_value' => '2022-02-15']);

        $this->assertEquals(Carbon::parse('2022-02-15'), $item->valueAsDate());
    }

    public function testThrowsExceptionReturningValueAsDateForNullValue(): void
    {
        $item = $this->makeItem('test-item', ['config_value' => null]);

        $this->expectException(ParseValueException::class);

        $item->valueAsDate();
    }

    public function testThrowsExceptionReturningValueAsDateForInvalidDateString(): void
    {
        $item = $this->makeItem('test-item', ['config_value' => 'blue']);

        $this->expectException(ParseValueException::class);

        $item->valueAsDate();
    }

    public function testReturnsDataFromYamlValue(): void
    {
        $item = $this->makeItem('test-item', ['config_value' => "---\n- green\n- red"]);

        $this->assertEquals(['green', 'red'], $item->valueDataFromYaml());
    }

    public function testThrowsExceptionReturningValueFromYamlForInvalidData(): void
    {
        $item = $this->makeItem('test-item', ['config_value' => "---\n{green\n- red"]);

        $this->expectException(ParseValueException::class);

        $item->valueDataFromYaml();
    }

    public function testReturnsDataFromNullYamlValue(): void
    {
        $item = $this->makeItem('test-item', ['config_value' => null]);

        $this->assertNull($item->valueDataFromYaml());
    }

    public function testReturnsDataFromEmptyYamlValue(): void
    {
        $item = $this->makeItem('test-item', ['config_value' => '']);

        $this->assertEquals('', $item->valueDataFromYaml());
    }
}
