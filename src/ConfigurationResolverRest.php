<?php

namespace MiamiOH\ConfigManager;

use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use MiamiOH\ConfigManager\Exceptions\ClientException;
use MiamiOH\ConfigManager\Exceptions\NotAuthorizedException;
use MiamiOH\RESTng\Client\Agent;

class ConfigurationResolverRest extends ConfigurationResolver
{
    public const CONFIG_API_BASE = '/api/config/v1';

    /**
     * @var Agent
     */
    private $agent;
    /**
     * @var DataParser
     */
    private $parser;
    /**
     * @var int
     */
    private $cacheSeconds;

    public function __construct(Agent $agent, DataParser $parser, int $cacheSeconds)
    {
        $this->agent = $agent;
        $this->parser = $parser;
        $this->cacheSeconds = $cacheSeconds;
    }

    protected function getCategory(string $application, string $category): Collection
    {
        $this->refreshCategory($application, $category);

        return Cache::get($this->categoryCacheKey($application, $category));
    }

    private function refreshCategory(string $application, string $category): void
    {
        if (Cache::has($this->categoryCacheKey($application, $category))) {
            return;
        }


        Cache::remember($this->categoryCacheKey($application, $category), $this->cacheSeconds, function () use ($application, $category) {
            $response = $this->agent->run(
                new Request('GET', sprintf('%s/%s?format=complete&category=%s', self::CONFIG_API_BASE, $application, $category))
            );

            if ($response->status() === 200) {
                return $this->parser->categoryFromData($response->data());
            }

            if ($response->status() === 401) {
                throw new NotAuthorizedException(sprintf('Request for %s -> %s is not authorized', $application, $category));
            }

            throw new ClientException(sprintf('Request for %s -> %s failed with %s', $application, $category, $response->status()));
        });
    }
}
