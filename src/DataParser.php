<?php

namespace MiamiOH\ConfigManager;

use Illuminate\Support\Collection;

class DataParser
{
    public function categoryFromData(array $data): Collection
    {
        return array_reduce(array_keys($data), function (Collection $c, string $name) use ($data) {
            $c->put($name, ConfigurationItem::fromDataArray($name, $data[$name]));
            return $c;
        }, new Collection());
    }
}
