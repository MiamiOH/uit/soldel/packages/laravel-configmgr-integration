<?php

namespace MiamiOH\ConfigManager\Testing;

use MiamiOH\ConfigManager\ConfigurationResolverArray;

/**
 * @codeCoverageIgnore
 */
trait GetConfiguration
{
    public function withConfiguration(array $configuration): void
    {
        /** @var ConfigurationResolverArray $confResolver */
        $confResolver = $this->app->make(ConfigurationResolverArray::class);

        $confResolver->withConfiguration($configuration);
    }
}
