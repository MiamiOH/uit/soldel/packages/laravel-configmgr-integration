<?php

namespace MiamiOH\ConfigManager;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

/**
 * @codeCoverageIgnore
 */
class ConfigManagerServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/config-mgr.php' => config_path('config-mgr.php'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config-mgr.php', 'config-mgr');

        if (App::environment('testing')) {
            $this->app->singleton(ConfigurationResolverArray::class);
            $this->app->singleton(ConfigurationResolver::class, ConfigurationResolverArray::class);
        } else {
            $this->app->bind(ConfigurationResolver::class, ConfigurationResolverRest::class);
            $this->app->when(ConfigurationResolverRest::class)
                ->needs('$cacheSeconds')
                ->give(config('config-mgr.cache-seconds'));
        }
    }
}
