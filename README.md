# Config Manager Integration Package for Laravel 8+

## Installation

The minimum supported version is Laravel 8.

Make sure that the satis repo has been added to your composer.json file:

```
    "repositories": [
        {
            "type": "composer",
            "url": "https://satis.itapps.miamioh.edu/miamioh"
        }
    ]
```

Afterwards, run this in your app directory:

```
composer require miamioh/laravel-configmgr-integration
```

Optional: Publish the config file by running:

```
php artisan vendor:publish --provider 'MiamiOH\ConfigManager\ConfigManagerServiceProvider'
```

## Upgrading from 2.x to 3.x

When upgrading from 2.x to 3.x, please be aware of the following changes:

### Configuration authorization (Impact: Very High)

The previous release of this package used direct database access to query configuration items. That ability has been completely removed and replaced by access through authenticated REST resources. This also means that authorization is required to access the  necessary CAM values. The requests are made using the `RESTng PHP Client` (described below) and requires a valid RESTng username/password.

If your application does not already use a RESTng user, you will need to have a new user created and provisioned.

The RESTng user must be authorized with at least `view` access in CAM to the application(s) and category(ies) from which it will fetch configuration items.

### Public interface changes (Impact: Very High)

All public methods for interacting with the `ConfigurationService` are different in this release. There is no backward compatibility. Your application will need to be fully updated to use the new methods. While this is potentially a substantial amount of work, we feel the new public interface is more user friendly and offers advantages over the previous interface.

### Configuration file overhaul (Impact: Medium)

The package configuration file has been renamed from `config/config.php` to `config/config-mgr.php` and the contents have been completely overhauled. You should delete the old file. It is not necessary to publish the new configuration file. All options can be set through env variables. See the `Configuration` sections below.

## Configuration

### Environment Configuration

The following can be set in your `.env`:

```
CONFIGMGR_DEFAULT_APPLICATION="My Application"
CONFIGMGR_DEFAULT_CATEGORY="My Category"
CONFIGMGR_CACHE_SECONDS=300
```

The default application and category are used when the `fromApplication()` and `fromCategory()` methods do not specify an alternative.

The cache seconds controls how long resolved configuration values are cached.

### RESTng Agent Configuration

This package relies on the [PHP RESTng Client](https://gitlab.com/MiamiOH/uit/soldel/packages/restng-php-client) package for the underlying HTTP request handling. The `ConfigurationResolver` class takes a `\MiamiOH\RESTng\Client\Agent` object as a dependency and uses it to call the RESTng configuration resource. You will need to ensuring a suitable `Agent` object can be resolved by the Laravel container. In the simplest form, this can be done by ensure a valid `Endpoint` object is available:

```php
$this->app->bind(\MiamiOH\RESTng\Client\Endpoint::class, function () {
    return new \MiamiOH\RESTng\Client\Endpoint(
        config('restng.restngUrl'),
        config('restng.credentials.token.default.username'),
        config('restng.credentials.token.default.password'),
    );
});
```

**Note**: Use the proper `config()` values based on your application's configuration.

## Usage

The primary usage is via the `ConfigurationService` class. You should inject or otherwise resolve this from the Laravel container.

### ConfigurationService

The basic usage of the service is to specify the application and category, and then fetch the value for a given key, as shown below:

```php
public function handle (\MiamiOH\ConfigManager\ConfigurationService $configurationService): void
{
    $value = $configurationService
                ->fromApplication('My Application')
                ->fromCategory('My Category')
                ->getItemValue('item-key');
}
```

The `fromApplication()` and `fromCategory()` methods return a new `ConfigurationService` object and do not alter the original object state. You may assign the resulting `ConfigurationService` object and call it repeatedly:

```php
$category = $configurationService
            ->fromApplication('My Application')
            ->fromCategory('My Category');

$item1 = $category->getItemValue('item1');
$item2 = $category->getItemValue('item2');
...
```

The `ConfigurationService` object will use the default application and category, if set, allowing you to simply fetch items:

```php
# in the .env file:
# CONFIGMGR_DEFAULT_APPLICATION="My Application"
# CONFIGMGR_DEFAULT_CATEGORY="My Category"

$value = $configurationService->getItemValue('item-key');
```

The `getItemValue()` method accepts a default string value:

```php
$value = $configurationService->getItemValue('item-key', 'green');
```

*Note*: The default is only returned if the requested key is not found.

*Note*: The default value must be a string.

### Configuration Value Convenience Methods

The `CategoryService` class provides additional helper methods for transforming item values to known types:

```php
// bool
$value = $configurationService->getItemValueAsBool('item-key');

// Carbon
$value = $configurationService->getItemValueAsDate('item-key');

// Data array (item value must be valid yaml)
$value = $configurationService->getItemValueFromYaml('item-key');
```

The `getItemValueAsBool()` method returns `true` for the string values 'true', 't', 'yes', or 'y' (case-insensitive). It will return `false` for any other value.

The `getItemValueAsDate()` method performs a `Carbon::parse()` on the item value, which must be a valid, parsable date string. A `\MiamiOH\ConfigManager\Exceptions\ParseValueException` exception will be thrown if Carbon cannot parse the string as a date. An empty value will also result in a `ParseValueException`.

The `getItemValueFromYaml()` method uses the `yaml_parse()` function to produce a data structure from the given value. A `\MiamiOH\ConfigManager\Exceptions\ParseValueException` exception will be thrown if parse attempt fails. An empty value will be returned as `null`.

### Configuration Item Collections

In addition to retrieving individual configuration items, you can also retrieve entire categories.

```php
// Get a Collection of key/value pairs with values as strings
$itemList = $configurationService
            ->fromApplication('My Application')
            ->fromCategory('My Category')
            ->getItemValues();

$value= $itemList->get('item-key');
// or
$value = $itemList['item-key'];

// Get a Collection of ConfigurationItems
$items = $configurationService
            ->fromApplication('My Application')
            ->fromCategory('My Category')
            ->getItems();

$value = $items->get('item-key')->value();
```

Both the `getItemValues()` and `getItems()` methods accept a callback which will be used to filter the resulting collection.

```php
$itemList = $configurationService
            ->fromApplication('My Application')
            ->fromCategory('My Category')
            ->getItemValues(function (ConfigurationItem $item) {
                // use any method to return a boolean
                return in_array($item->name(), ['key1', 'key2']);
            });
```

### Combining Item Values

Configuration item values are limited in size by the underlying database column. In some cases, this may present a constraint on the needs of your application. This package provides two convenience methods to combine the values of multiple configuration items into a single result. To use this method, you must create a `source` configuration item which list the component items in the order you which them to be merged. The format of the item value must be YAML and the names must match exactly. The package will fetch the `source` list item and loop over the key names, fetching each component and combining them.

In the following examples, `list-items` is the key of the item containing a yaml encoded list of other item keys. For example:

```yaml
---
- key1
- key2
```

#### Imploding Item Values to String

The `implodeItemValues()` method implodes the string value of each item. The values will be imploded in the order of the keys in the source item value. The delimiter is optional and defaults to `null`. Any value valid for PHP's `implode` function may be used. The method also accepts an optional third argument of key/value replacements which will be applied to the values.

```php
// Combine to a single string using default delimiter (null)
$resultString = $configurationService
            ->fromApplication('My Application')
            ->fromCategory('My Category')
            ->implodeItemValues('list-items');

// Combine to a single string using explicit delimiter
$resultString = $configurationService
            ->fromApplication('My Application')
            ->fromCategory('My Category')
            ->implodeItemValues('list-items', ', ');

// Combine to a single string with replacements
$resultString = $configurationService
            ->fromApplication('My Application')
            ->fromCategory('My Category')
            ->implodeItemValues('list-items', ', ', ['color' => 'green', 'size' => 'large', 'flavor' => 'berry']);
```

#### Merging Item Values as Data to Array

The `mergeItemData()` method merges the data array derived from each item. All items referenced in the source list must themselves contain valid YAML. The YAML string will be parsed to data arrays and merged using PHP's `array_merge` function. All considerations when using `array_merge` should be considered.

```php
// Merge to a single data array
$mergedData = $configurationService
            ->fromApplication('My Application')
            ->fromCategory('My Category')
            ->mergeItemData('list-items');
```

### ConfigurationItem

The `ConfigurationService` object can return `ConfigurationItem` objects in addition to the value. The `ConfigurationItem` class provides additional data and methods related to the item:

```php
$item = $configurationService->getItem('item-key');

$item->name();
$item->application();
$item->category();
$item->description();
$item->dataStructure();
$item->dataType();
$item->value();
$item->updatedAt();
$item->valueAsBool();
$item->valueAsDate();
$item->valueDataFromYaml();
```

The `value()` method accepts an array of key/value pairs to use as replacements:

```php
# item value: Hello {who}!

$item = $configurationService->getItem('item-key');

$value = $item->value(['who' => 'world']);

# result: Hello world!
```

These methods can be chained for clarity:

```php
$value = $configurationService
            ->fromApplication('My Application')
            ->fromCategory('My Category')
            ->getItem('item-key')
            ->value(['who' => 'world']);
```

## Caching

The package fetches entire categories in a single request and caches the results. Configuration caching is enabled by default and uses the configured Laravel cache service. Cache entries are keyed by application and category.

The default cache lifetime is 3600 seconds (one hour). The cache lifetime can be changed with the `CONFIGMGR_CACHE_SECONDS` environment variable. You should be aware, and ensure stakeholders are aware, that configuration changes in test and production may take up to the cache lifetime to apply. If an urgent change is required, Laravel `artisan` can be used to clear the cache:

```php
php artisan cache:clear
```

Note that if a local cache driver (such as `file`) is used, the `cache:clear` command must be run on each server.

During development, you may want to set your cache driver to `array` in order to ensure you are always working with the most up-to-date configuration data.

## Testing Helpers

This package includes a trait to make testing easier. During tests, you don't want the package to actually make HTTP requests to the REST configuration service. Using the trait, you can set your desired configuration values and the `ConfigurationService` object will respond appropriately.
```php
use MiamiOH\ConfigManager\Testing\GetConfiguration;

class MainPageTest extends FeatureTestCase
{
    use GetConfiguration;

    public function testApplicationAuthorization()
    {
        $this->withConfiguration([
            'My Application' => [
                'My Category' => [
                    [
                        'highlight-color', // key
                        'red', // value
                        'scalar' // data structure (default scalar)
                        'string' // data type (default string)
                    ],
                    ['heading', 'Heading Value'],
                    ['intro', 'Welcome to the app!'],
                ],
            ],
            ...
        ]);

        ...
    }
}
```

The `withConfiguration` method accepts a nested array of applications, categories and configurations. The configuration data array must conform to:

```php
[key, value, structure, type]
```

The `key` is the expected configuration key to match. The `value` should be the string representation of the value as it would be entered in CAM. The `value` is optional and will be `null` if not provided. Both `structure` and `type` are optional (and rarely used), defaulting to `scalar` and `string` respectively.

## Running Unit Tests

This package uses Illuminate packages and must be tested with different versions for maximum compatibility. In most cases, the Illuminate package maintainers are able to provide forward and backward compatibility. This package is known to work with Illuminate 6 through 11. This range covers different versions of PHP and requires a matrix approach to testing. Manually executing the tests is possible using the includes `tests.sh` shell script in conjunction with our PHP build images.

A prerequisite for running the tests is being able to run the necessary containers. Once you have installed Docker Desktop (or the container platform of your choice), make sure you can run:

```bash
docker run -it --rm -v $(pwd):/opt/project -v ~/.composer:/root/.composer miamioh/php:7.3-devtools bash
```

This will bring to a shell in the PHP 7.3 image which is suitable for running tests. Exit the shell and run the same command with the PHP 8.1 or 8.2 image.

Now use the appropriate version of PHP to run the package tests with different versions of Illuminate:

```bash
# PHP 7.3

bash tests.sh --illuminate 8.0 

# PHP 8.1

bash tests.sh --illuminate 8.0
bash tests.sh --illuminate 9.0 
bash tests.sh --illuminate 10.0 

# PHP 8.2
bash tests.sh --illuminate 11.0
```

**!!IMPORTANT!!** The test script alters the `composer.json` file as it executes tests with different versions. The script backs up the original and restores it after composer runs, but you must be careful not to commit changes which may leak. Carefully review any changes to the `composer.json` file before committing them.
