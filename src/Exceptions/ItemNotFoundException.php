<?php

namespace MiamiOH\ConfigManager\Exceptions;

class ItemNotFoundException extends ConfigurationException
{
}
