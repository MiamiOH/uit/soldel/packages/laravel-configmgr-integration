<?php

namespace Test;

use Carbon\Carbon;
use Illuminate\Config\Repository;
use Illuminate\Support\Collection;
use MiamiOH\ConfigManager\ConfigurationItem;
use MiamiOH\ConfigManager\ConfigurationResolver;
use MiamiOH\ConfigManager\ConfigurationService;
use MiamiOH\ConfigManager\Exceptions\ItemNotFoundException;
use PHPUnit\Framework\MockObject\MockObject;

class ConfigurationServiceTest extends TestCase
{
    /**
     * @var ConfigurationResolver|MockObject
     */
    private $resolver;
    /**
     * @var Repository|MockObject
     */
    private $appConfig;

    private $application;
    private $category;

    public function setUp(): void
    {
        parent::setUp();

        $this->resolver = $this->createMock(ConfigurationResolver::class);
        $this->appConfig = $this->createMock(Repository::class);

        $this->appConfig->method('get')
            ->willReturnMap([
                ['config-mgr.default-application', null, 'My Application'],
                ['config-mgr.default-category', null, 'My Category'],
            ]);

        $this->application = $this->faker->word();
        $this->category = $this->faker->word();
    }

    public function testUsesDefaultApplicationAndCategoryWhenNotGiven(): void
    {
        $service = new ConfigurationService($this->resolver, $this->appConfig);

        $this->resolver->expects($this->once())->method('getItem')
            ->with(
                $this->equalTo('My Application'),
                $this->equalTo('My Category'),
                $this->equalTo('test-key')
            )->willReturn($this->makeItem());

        $service->getItem('test-key');
    }

    public function testUsesDefaultApplicationAndCategoryWhenNull(): void
    {
        $service = new ConfigurationService($this->resolver, $this->appConfig);

        $this->resolver->expects($this->once())->method('getItem')
            ->with(
                $this->equalTo('My Application'),
                $this->equalTo('My Category'),
                $this->equalTo('test-key')
            )->willReturn($this->makeItem());

        $service->getItem('test-key');
    }

    public function testUsesDefaultCategoryWhenNotGiven(): void
    {
        $service = new ConfigurationService($this->resolver, $this->appConfig, 'Test Application');

        $this->resolver->expects($this->once())->method('getItem')
            ->with(
                $this->equalTo('Test Application'),
                $this->equalTo('My Category'),
                $this->equalTo('test-key')
            )->willReturn($this->makeItem());

        $service->getItem('test-key');
    }

    public function testUsesDefaultCategoryWhenNull(): void
    {
        $service = new ConfigurationService($this->resolver, $this->appConfig, 'Test Application', null);

        $this->resolver->expects($this->once())->method('getItem')
            ->with(
                $this->equalTo('Test Application'),
                $this->equalTo('My Category'),
                $this->equalTo('test-key')
            )->willReturn($this->makeItem());

        $service->getItem('test-key');
    }

    public function testGetsConfigurationItemValueFromResolver(): void
    {
        $service = $this->makeConfigurationService();

        $this->resolver->expects($this->once())->method('getItem')
            ->with(
                $this->equalTo($this->application),
                $this->equalTo($this->category),
                $this->equalTo('test-key')
            )
            ->willReturn($this->makeItem('test-key', ['config_value' => 'test value']));

        $value = $service->getItemValue('test-key');

        $this->assertEquals('test value', $value);
    }

    public function testGetsConfigurationItemValueFromResolverWithGivenApplication(): void
    {
        $service = $this->makeConfigurationService()->fromApplication('Other Application');

        $this->resolver->expects($this->once())->method('getItem')
            ->with(
                $this->equalTo('Other Application'),
                $this->equalTo($this->category),
                $this->equalTo('test-key')
            )
            ->willReturn($this->makeItem('test-key', ['config_value' => 'test value']));

        $value = $service->getItemValue('test-key');

        $this->assertEquals('test value', $value);
    }

    public function testGetsConfigurationItemValueFromResolverWithGivenCategory(): void
    {
        $service = $this->makeConfigurationService()->fromCategory('Other Category');

        $this->resolver->expects($this->once())->method('getItem')
            ->with(
                $this->equalTo($this->application),
                $this->equalTo('Other Category'),
                $this->equalTo('test-key')
            )
            ->willReturn($this->makeItem('test-key', ['config_value' => 'test value']));

        $value = $service->getItemValue('test-key');

        $this->assertEquals('test value', $value);
    }

    public function testReturnsDefaultValueIfItemNotFound(): void
    {
        $service = $this->makeConfigurationService()->fromApplication('Other Application');

        $this->resolver->method('getItem')
            ->willThrowException(new ItemNotFoundException());

        $default = $this->faker->word();

        $value = $service->getItemValue('test-key', $default);

        $this->assertEquals($default, $value);
    }

    public function testThrowsExceptionIfItemNotFoundAndNoDefault(): void
    {
        $service = $this->makeConfigurationService()->fromApplication('Other Application');

        $this->resolver->method('getItem')
            ->willThrowException(new ItemNotFoundException());

        $this->expectException(ItemNotFoundException::class);

        $service->getItemValue('test-key');
    }

    public function testReturnsConfigurationItem(): void
    {
        $service = $this->makeConfigurationService();

        $item = $this->makeItem('test-key', ['config_value' => 'test value']);

        $this->resolver->expects($this->once())->method('getItem')
            ->with(
                $this->equalTo($this->application),
                $this->equalTo($this->category),
                $this->equalTo('test-key')
            )
            ->willReturn($item);

        $this->assertSame($item, $service->getItem('test-key'));
    }

    /**
     * @dataProvider itemValueAsBoolChecks
     */
    public function testGetsConfigurationItemValueAsBool(string $value, bool $expected): void
    {
        $service = $this->makeConfigurationService();

        $item = $this->makeItem('test-key', ['config_value' => $value]);

        $this->resolver->method('getItem')->willReturn($item);

        $this->assertEquals($expected, $service->getItemValueAsBool('test-key'));
    }

    public static function itemValueAsBoolChecks(): array
    {
        return [
            'true' => ['true', true],
            'false' => ['false', false],
        ];
    }

    public function testGetsConfigurationItemValueAsDate(): void
    {
        $service = $this->makeConfigurationService();

        $item = $this->makeItem('test-key', ['config_value' => '2022-03-07']);

        $this->resolver->method('getItem')->willReturn($item);

        $this->assertEquals(Carbon::parse('2022-03-07'), $service->getItemValueAsDate('test-key'));
    }

    public function testGetsDataFromConfigurationItemYaml(): void
    {
        $service = $this->makeConfigurationService();

        $data = [
            'color' => 'green',
            'size' => 'small',
        ];

        $item = $this->makeItem('test-key', ['config_value' => yaml_emit($data)]);

        $this->resolver->method('getItem')->willReturn($item);

        $this->assertEquals($data, $service->getItemValueFromYaml('test-key'));
    }

    public function testReturnsCategoryItemValuesAsCollection(): void
    {
        $service = $this->makeConfigurationService();

        $this->resolver->expects($this->once())->method('getItems')
            ->with(
                $this->equalTo($this->application),
                $this->equalTo($this->category)
            )
            ->willReturn(new Collection([
                'test-item' => $this->makeItem('test-item', ['config_value' => 'test value']),
                'test-item2' => $this->makeItem('test-item2', ['config_value' => 'test value2']),
            ]));

        $items = $service->getItemValues();

        $this->assertCount(2, $items);
        $this->assertTrue($items->has('test-item'));
        $this->assertTrue($items->has('test-item2'));
        $this->assertEquals('test value', $items->get('test-item'));
        $this->assertEquals('test value2', $items->get('test-item2'));
    }

    public function testReturnsFilteredCategoryItemValuesAsCollection(): void
    {
        $service = $this->makeConfigurationService();

        $this->resolver->expects($this->once())->method('getItems')
            ->with(
                $this->equalTo($this->application),
                $this->equalTo($this->category)
            )
            ->willReturn(new Collection([
                'test-item' => $this->makeItem('test-item', ['config_value' => 'test value']),
                'test-item2' => $this->makeItem('test-item2', ['config_value' => 'test value2']),
                'test-item3' => $this->makeItem('test-item3', ['config_value' => 'test value3']),
            ]));

        $items = $service->getItemValues(function (ConfigurationItem $item) {
            return in_array($item->name(), ['test-item2', 'test-item3']);
        });

        $this->assertCount(2, $items);
        $this->assertTrue($items->has('test-item2'));
        $this->assertTrue($items->has('test-item3'));
        $this->assertEquals('test value2', $items->get('test-item2'));
        $this->assertEquals('test value3', $items->get('test-item3'));
    }

    public function testReturnsCategoryItemsAsCollection(): void
    {
        $service = $this->makeConfigurationService();

        $this->resolver->expects($this->once())->method('getItems')
            ->with(
                $this->equalTo($this->application),
                $this->equalTo($this->category)
            )
            ->willReturn(new Collection([
                'test-item' => $this->makeItem('test-item', ['config_value' => 'test value']),
                'test-item2' => $this->makeItem('test-item2', ['config_value' => 'test value2']),
            ]));

        $items = $service->getItems();

        $this->assertCount(2, $items);
        $this->assertTrue($items->has('test-item'));
        $this->assertTrue($items->has('test-item2'));
        $this->assertEquals('test value', $items->get('test-item')->value());
        $this->assertEquals('test value2', $items->get('test-item2')->value());
    }

    public function testReturnsFilteredCategoryItemsAsCollection(): void
    {
        $service = $this->makeConfigurationService();

        $this->resolver->expects($this->once())->method('getItems')
            ->with(
                $this->equalTo($this->application),
                $this->equalTo($this->category)
            )
            ->willReturn(new Collection([
                'test-item' => $this->makeItem('test-item', ['config_value' => 'test value']),
                'test-item2' => $this->makeItem('test-item2', ['config_value' => 'test value2']),
                'test-item3' => $this->makeItem('test-item3', ['config_value' => 'test value3']),
            ]));

        $items = $service->getItems(function (ConfigurationItem $item) {
            return in_array($item->name(), ['test-item2', 'test-item3']);
        });

        $this->assertCount(2, $items);
        $this->assertTrue($items->has('test-item2'));
        $this->assertTrue($items->has('test-item3'));
        $this->assertEquals('test value2', $items->get('test-item2')->value());
        $this->assertEquals('test value3', $items->get('test-item3')->value());
    }

    public function testImplodesCategoryItems(): void
    {
        $service = $this->makeConfigurationService();

        $this->resolver->expects($this->exactly(4))->method('getItem')
            ->willReturnMap([
                [
                    $this->application,
                    $this->category,
                    'list-items',
                    $this->makeItem('list-items', ['config_value' => yaml_emit(['test-item', 'test-item2', 'test-item3'])])
                ],
                [
                    $this->application,
                    $this->category,
                    'test-item',
                    $this->makeItem('test-item', ['config_value' => 'test value'])
                ],
                [
                    $this->application,
                    $this->category,
                    'test-item2',
                    $this->makeItem('test-item2', ['config_value' => 'test value2'])
                ],
                [
                    $this->application,
                    $this->category,
                    'test-item3',
                    $this->makeItem('test-item3', ['config_value' => 'test value3'])
                ],
            ]);

        $joined = $service->implodeItemValues('list-items', ', ');

        $this->assertEquals('test value, test value2, test value3', $joined);
    }

    public function testImplodesCategoryItemsOnNull(): void
    {
        $service = $this->makeConfigurationService();

        $this->resolver->expects($this->exactly(4))->method('getItem')
            ->willReturnMap([
                [
                    $this->application,
                    $this->category,
                    'list-items',
                    $this->makeItem('list-items', ['config_value' => yaml_emit(['test-item', 'test-item2', 'test-item3'])])
                ],
                [
                    $this->application,
                    $this->category,
                    'test-item',
                    $this->makeItem('test-item', ['config_value' => 'test value'])
                ],
                [
                    $this->application,
                    $this->category,
                    'test-item2',
                    $this->makeItem('test-item2', ['config_value' => 'test value2'])
                ],
                [
                    $this->application,
                    $this->category,
                    'test-item3',
                    $this->makeItem('test-item3', ['config_value' => 'test value3'])
                ],
            ]);

        $joined = $service->implodeItemValues('list-items');

        $this->assertEquals('test valuetest value2test value3', $joined);
    }

    public function testPerformsReplacementsWhenImplodingCategoryItems(): void
    {
        $service = $this->makeConfigurationService();

        $this->resolver->expects($this->exactly(4))->method('getItem')
            ->willReturnMap([
                [
                    $this->application,
                    $this->category,
                    'list-items',
                    $this->makeItem('list-items', ['config_value' => yaml_emit(['test-item', 'test-item2', 'test-item3'])])
                ],
                [
                    $this->application,
                    $this->category,
                    'test-item',
                    $this->makeItem('test-item', ['config_value' => 'test {color}'])
                ],
                [
                    $this->application,
                    $this->category,
                    'test-item2',
                    $this->makeItem('test-item2', ['config_value' => 'test {size}'])
                ],
                [
                    $this->application,
                    $this->category,
                    'test-item3',
                    $this->makeItem('test-item3', ['config_value' => 'test {flavor}'])
                ],
            ]);

        $joined = $service->implodeItemValues(
            'list-items',
            ', ',
            ['color' => 'green', 'size' => 'large', 'flavor' => 'berry']
        );

        $this->assertEquals('test green, test large, test berry', $joined);
    }

    public function testMergesCategoryItems(): void
    {
        $service = $this->makeConfigurationService();

        $this->resolver->expects($this->exactly(4))->method('getItem')
            ->willReturnMap([
                [
                    $this->application,
                    $this->category,
                    'list-items',
                    $this->makeItem('list-items', ['config_value' => yaml_emit(['test-item', 'test-item2', 'test-item3'])])
                ],
                [
                    $this->application,
                    $this->category,
                    'test-item',
                    $this->makeItem('test-item', ['config_value' => yaml_emit(['color' => 'green'])])
                ],
                [
                    $this->application,
                    $this->category,
                    'test-item2',
                    $this->makeItem('test-item2', ['config_value' => yaml_emit(['size' => 'large'])])
                ],
                [
                    $this->application,
                    $this->category,
                    'test-item3',
                    $this->makeItem('test-item3', ['config_value' => yaml_emit(['flavor' => 'berry'])])
                ],
            ]);

        $merged = $service->mergeItemData('list-items');

        $this->assertEquals([
            'color' => 'green',
            'size' => 'large',
            'flavor' => 'berry',
        ], $merged);
    }

    private function makeConfigurationService(): ConfigurationService
    {
        return new ConfigurationService(
            $this->resolver,
            $this->appConfig,
            $this->application,
            $this->category
        );
    }
}
