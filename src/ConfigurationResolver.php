<?php

namespace MiamiOH\ConfigManager;

use Illuminate\Support\Collection;
use MiamiOH\ConfigManager\Exceptions\ItemNotFoundException;

abstract class ConfigurationResolver
{
    public const CONFIG_CACHE_KEY_FORMAT = 'config-mgr-category: %s / %s';

    /**
     * @param string $application
     * @param string $category
     * @param string $key
     * @return ConfigurationItem
     * @throws ItemNotFoundException
     */
    public function getItem(string $application, string $category, string $key): ConfigurationItem
    {
        $items = $this->getCategory($application, $category);

        if ($items->has($key)) {
            return $items->get($key);
        }

        throw new ItemNotFoundException(sprintf('Request item %s was not found in %s -> %s', $key, $application, $category));
    }

    /**
     * @param string $application
     * @param string $category
     * @return Collection
     */
    public function getItems(string $application, string $category): Collection
    {
        return $this->getCategory($application, $category);
    }

    public function categoryCacheKey(string $application, string $category): string
    {
        return sprintf(self::CONFIG_CACHE_KEY_FORMAT, $application, $category);
    }

    abstract protected function getCategory(string $application, string $category): Collection;
}
