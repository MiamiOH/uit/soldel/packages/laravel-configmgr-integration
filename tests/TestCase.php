<?php

namespace Test;

use Carbon\Carbon;
use Faker\Factory;
use MiamiOH\ConfigManager\ConfigurationItem;

class TestCase extends \Orchestra\Testbench\TestCase
{
    /**
     * @var \Faker\Generator
     */
    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->faker = Factory::create();
    }

    protected function makeItemData(string $name = null, array $overrides = []): array
    {
        $name = $name ?? $this->faker->word();

        return [
            $name => array_merge([
                'config_application' => $this->faker->word(),
                'config_category' => $this->faker->word(),
                'config_desc' => $this->faker->word(),
                'config_data_structure' => 'scalar',
                'config_data_type' => 'text',
                'config_value' => $this->faker->word(),
                'activity_date' => Carbon::now()->subMonth()->format('Y-m-d'),
            ], $overrides)
        ];
    }

    protected function makeItem(string $name = null, array $overrides = []): ConfigurationItem
    {
        $name = $name ?? $this->faker->word();

        $data = $this->makeItemData($name, $overrides);

        return new ConfigurationItem(
            $name,
            $data[$name]['config_application'],
            $data[$name]['config_category'],
            $data[$name]['config_desc'],
            $data[$name]['config_data_structure'],
            $data[$name]['config_data_type'],
            $data[$name]['config_value'],
            Carbon::parse($data[$name]['activity_date'])
        );
    }
}
