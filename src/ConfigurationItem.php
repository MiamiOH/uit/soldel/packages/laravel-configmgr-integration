<?php

namespace MiamiOH\ConfigManager;

use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use MiamiOH\ConfigManager\Exceptions\ParseValueException;

class ConfigurationItem
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $application;
    /**
     * @var string
     */
    private $category;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $dataStructure;
    /**
     * @var string
     */
    private $dataType;
    /**
     * @var string
     */
    private $value;
    /**
     * @var Carbon
     */
    private $updatedAt;

    public function __construct(
        string $name,
        string $application,
        string $category,
        ?string $description,
        string $dataStructure,
        string $dataType,
        ?string $value,
        Carbon $updatedAt
    ) {
        $this->name = $name;
        $this->application = $application;
        $this->category = $category;
        $this->description = $description ?? '';
        $this->dataStructure = $dataStructure;
        $this->dataType = $dataType;
        $this->value = $value ?? '';
        $this->updatedAt = $updatedAt;
    }

    public static function fromDataArray(string $name, array $data): self
    {
        return new self(
            $name,
            $data['config_application'],
            $data['config_category'],
            $data['config_desc'],
            $data['config_data_structure'],
            $data['config_data_type'],
            $data['config_value'],
            Carbon::parse($data['activity_date'])
        );
    }

    public function name(): string
    {
        return $this->name;
    }

    public function application(): string
    {
        return $this->application;
    }

    public function category(): string
    {
        return $this->category;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function dataStructure(): string
    {
        return $this->dataStructure;
    }

    public function dataType(): string
    {
        return $this->dataType;
    }

    public function value(array $replacements = null): string
    {
        if (empty($replacements)) {
            return $this->value;
        }

        return str_replace(
            array_map(function (string $search) {
                return sprintf('{%s}', $search);
            }, array_keys($replacements)),
            array_values($replacements),
            $this->value
        );
    }

    public function updatedAt(): Carbon
    {
        return $this->updatedAt;
    }

    public function valueAsBool(): bool
    {
        if (empty($this->value)) {
            return false;
        }

        return in_array(strtolower($this->value), ['true', 't', 'yes', 'y']);
    }

    /**
     * @return Carbon
     * @throws ParseValueException
     */
    public function valueAsDate(): Carbon
    {
        if (empty($this->value)) {
            throw new ParseValueException('Cannot parse empty value to date');
        }

        try {
            return Carbon::parse($this->value);
        } catch (InvalidFormatException $e) {
            throw new ParseValueException(sprintf('Cannot parse value "%s" to date', $this->value));
        }
    }

    /**
     * @return array|null
     * @throws ParseValueException
     */
    public function valueDataFromYaml(): ?array
    {
        if (empty($this->value)) {
            return null;
        }

        try {
            return yaml_parse($this->value);
        } catch (\ErrorException $e) {
            throw new ParseValueException(sprintf('Error parsing yaml from: %s', $this->value), $e->getCode(), $e);
        }
    }
}
