<?php

namespace MiamiOH\ConfigManager;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class ConfigurationResolverArray extends ConfigurationResolver
{
    /** @var Collection */
    private $categories;

    public function __construct()
    {
        $this->categories = new Collection();
    }

    public function withConfiguration(array $configuration): void
    {
        $this->categories = array_reduce(array_keys($configuration), function (Collection $c, string $application) use ($configuration) {
            array_map(function (string $category) use ($c, $application, $configuration) {
                foreach ($configuration[$application][$category] as $configItem) {
                    $categoryKey = $this->categoryCacheKey($application, $category);

                    if (!$c->has($categoryKey)) {
                        $c->put($categoryKey, new Collection());
                    }

                    $c->get($categoryKey)->put($configItem[0], new ConfigurationItem(
                        $configItem[0],
                        $application,
                        $category,
                        null,
                        $configItem[2] ?? 'scalar',
                        $configItem[3] ?? 'string',
                        $configItem[1] ?? null,
                        Carbon::now()
                    ));
                }
            }, array_keys($configuration[$application]));

            return $c;
        }, new Collection());
    }

    protected function getCategory(string $application, string $category): Collection
    {
        $categoryKey = $this->categoryCacheKey($application, $category);

        if ($this->categories->has($categoryKey)) {
            return $this->categories->get($categoryKey);
        }

        return new Collection();
    }
}
