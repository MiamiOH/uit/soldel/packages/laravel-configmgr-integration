<?php

namespace MiamiOH\ConfigManager\Exceptions;

class ParseValueException extends ConfigurationException
{
}
