<?php

namespace Test;

use MiamiOH\ConfigManager\ConfigurationResolverArray;
use MiamiOH\ConfigManager\Exceptions\ClientException;
use MiamiOH\ConfigManager\Exceptions\ItemNotFoundException;

class ConfigurationResolverArrayTest extends TestCase
{
    /**
     * @var ConfigurationResolverArray
     */
    private $resolver;

    public function setUp(): void
    {
        parent::setUp();

        $this->resolver = new ConfigurationResolverArray();
    }

    public function testThrowsExceptionWhenRequestedKeyIsNotFoundInCategory(): void
    {
        $this->expectException(ItemNotFoundException::class);

        $this->resolver->getItem('MyApplication', 'MyCategory', 'test-item');
    }

    /**
     * @dataProvider configurationItemByNameChecks
     */
    public function testReturnConfigurationCategoryItem(string $key, string $expected): void
    {
        $this->resolver->withConfiguration([
            'MyApplication' => [
                'MyCategory' => [
                    ['test-item', 'test value'],
                    ['test-item2', 'test value2'],
                ]
            ]
        ]);

        $item = $this->resolver->getItem('MyApplication', 'MyCategory', $key);

        $this->assertEquals($expected, $item->value());
    }

    public static function configurationItemByNameChecks(): array
    {
        return [
            'first item' => ['test-item', 'test value'],
            'second item' => ['test-item2', 'test value2'],
        ];
    }

    public function testReturnConfigurationCategoryItemsInCollection(): void
    {
        $this->resolver->withConfiguration([
            'MyApplication' => [
                'MyCategory' => [
                    ['test-item', 'test value'],
                    ['test-item2', 'test value2'],
                ]
            ]
        ]);

        $items = $this->resolver->getItems('MyApplication', 'MyCategory');

        $this->assertCount(2, $items);
        $this->assertTrue($items->has('test-item'));
        $this->assertTrue($items->has('test-item2'));
        $this->assertEquals('test value', $items->get('test-item')->value());
        $this->assertEquals('test value2', $items->get('test-item2')->value());
    }
}
