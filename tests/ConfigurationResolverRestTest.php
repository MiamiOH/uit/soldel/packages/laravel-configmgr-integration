<?php

namespace Test;

use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use MiamiOH\ConfigManager\ConfigurationResolverRest;
use MiamiOH\ConfigManager\Exceptions\ClientException;
use MiamiOH\ConfigManager\Exceptions\ItemNotFoundException;
use MiamiOH\ConfigManager\Exceptions\NotAuthorizedException;
use MiamiOH\ConfigManager\DataParser;
use MiamiOH\RESTng\Client\Agent;
use MiamiOH\RESTng\Client\ResponseData;
use PHPUnit\Framework\MockObject\MockObject;

class ConfigurationResolverRestTest extends TestCase
{
    /**
     * @var ConfigurationResolverRest
     */
    private $resolver;

    /**
     * @var Agent|MockObject
     */
    private $agent;

    public function setUp(): void
    {
        parent::setUp();

        $this->agent = $this->createMock(Agent::class);

        $this->resolver = new ConfigurationResolverRest($this->agent, new DataParser(), 30);
    }

    public function testThrowsExceptionWhenRequestedKeyIsNotFoundInCategory(): void
    {
        $this->agent->expects($this->once())->method('run')
            ->willReturn(new ResponseData(200, $this->makeItemData('test-other-item', ['config_value' => 'test other value'])));

        $this->expectException(ItemNotFoundException::class);

        $this->resolver->getItem('MyApplication', 'MyCategory', 'test-item');
    }

    /**
     * @dataProvider configurationItemByNameChecks
     */
    public function testReturnConfigurationCategoryItem(string $key, string $expected): void
    {
        $this->agent->expects($this->once())->method('run')
            ->with($this->callback(function (Request $request) {
                $this->assertStringContainsString('MyApplication', $request->getUri()->getPath());
                $this->assertStringContainsString('MyCategory', $request->getUri()->getQuery());
                return true;
            }))
            ->willReturn(new ResponseData(200,
                array_merge(
                    $this->makeItemData('test-item', ['config_value' => 'test value']),
                    $this->makeItemData('test-item2', ['config_value' => 'test value2']),
                )
            ));

        $item = $this->resolver->getItem('MyApplication', 'MyCategory', $key);

        $this->assertEquals($expected, $item->value());
    }

    public static function configurationItemByNameChecks(): array
    {
        return [
            'first item' => ['test-item', 'test value'],
            'second item' => ['test-item2', 'test value2'],
        ];
    }

    public function testReturnConfigurationCategoryItemsInCollection(): void
    {
        $this->agent->expects($this->once())->method('run')
            ->with($this->callback(function (Request $request) {
                $this->assertStringContainsString('MyApplication', $request->getUri()->getPath());
                $this->assertStringContainsString('MyCategory', $request->getUri()->getQuery());
                return true;
            }))
            ->willReturn(new ResponseData(200,
                array_merge(
                    $this->makeItemData('test-item', ['config_value' => 'test value']),
                    $this->makeItemData('test-item2', ['config_value' => 'test value2']),
                )
            ));

        $items = $this->resolver->getItems('MyApplication', 'MyCategory');

        $this->assertCount(2, $items);
        $this->assertTrue($items->has('test-item'));
        $this->assertTrue($items->has('test-item2'));
        $this->assertEquals('test value', $items->get('test-item')->value());
        $this->assertEquals('test value2', $items->get('test-item2')->value());
    }

    public function testUsesCachedCategoryDataWhenPresent(): void
    {
        Cache::shouldReceive('has')->once()
            ->with($this->resolver->categoryCacheKey('MyApplication', 'MyCategory'))
            ->andReturn(true);

        Cache::shouldReceive('remember')->never();

        Cache::shouldReceive('get')->once()
            ->with($this->resolver->categoryCacheKey('MyApplication', 'MyCategory'))
            ->andReturn(new Collection([
                'test-item' => $this->makeItem('test-item', ['config_value' => 'test value']),
            ]));

        $this->agent->expects($this->never())->method('run');

        $value = $this->resolver->getItem('MyApplication', 'MyCategory', 'test-item');

        $this->assertEquals('test value', $value->value());
    }

    public function testThrowsExceptionWhenNotAuthorized(): void
    {
        $this->agent->expects($this->once())->method('run')
            ->willReturn(new ResponseData(401));

        $this->expectException(NotAuthorizedException::class);

        $this->resolver->getItem('MyApplication', 'MyCategory', 'test-item');
    }

    public function testThrowsExceptionWhenHttpRequestHasError(): void
    {
        $this->agent->expects($this->once())->method('run')
            ->willReturn(new ResponseData(500));

        $this->expectException(ClientException::class);

        $this->resolver->getItem('MyApplication', 'MyCategory', 'test-item');
    }
}
