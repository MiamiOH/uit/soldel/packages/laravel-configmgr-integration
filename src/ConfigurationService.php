<?php

namespace MiamiOH\ConfigManager;

use Carbon\Carbon;
use Illuminate\Config\Repository as AppConfig;
use Illuminate\Support\Collection;

class ConfigurationService
{
    /**
     * @var ConfigurationResolver
     */
    private $configurationResolver;

    /**
     * @var AppConfig
     */
    private $appConfig;

    /**
     * @var string
     */
    private $application;

    /**
     * @var string
     */
    private $category;

    public function __construct(
        ConfigurationResolver $configurationResolver,
        AppConfig             $appConfig,
        string                $application = null,
        string                $category = null
    ) {
        $this->configurationResolver = $configurationResolver;
        $this->appConfig = $appConfig;

        $this->application = $application ?? $appConfig->get('config-mgr.default-application');
        $this->category = $category ?? $appConfig->get('config-mgr.default-category');
    }

    public function fromApplication(string $application): self
    {
        return new self($this->configurationResolver, $this->appConfig, $application, $this->category);
    }

    public function fromCategory(string $category): self
    {
        return new self($this->configurationResolver, $this->appConfig, $this->application, $category);
    }

    /**
     * @param string $key
     * @param string|null $default
     * @return string
     * @throws Exceptions\ItemNotFoundException
     */
    public function getItemValue(string $key, string $default = null): string
    {
        try {
            return $this->getItem($key)->value();
        } catch (Exceptions\ItemNotFoundException $e) {
            if (null === $default) {
                throw $e;
            }
        }

        return $default;
    }

    /**
     * @param string $key
     * @return ConfigurationItem
     * @throws Exceptions\ItemNotFoundException
     */
    public function getItem(string $key): ConfigurationItem
    {
        return $this->configurationResolver->getItem($this->application, $this->category, $key);
    }

    /**
     * @param string $key
     * @return bool
     * @throws Exceptions\ItemNotFoundException
     */
    public function getItemValueAsBool(string $key): bool
    {
        return $this->getItem($key)->valueAsBool();
    }

    /**
     * @param string $key
     * @return Carbon
     * @throws Exceptions\ItemNotFoundException
     * @throws Exceptions\ParseValueException
     */
    public function getItemValueAsDate(string $key): Carbon
    {
        return $this->getItem($key)->valueAsDate();
    }

    /**
     * @param string $key
     * @return array
     * @throws Exceptions\ItemNotFoundException
     * @throws Exceptions\ParseValueException
     */
    public function getItemValueFromYaml(string $key): array
    {
        return $this->getItem($key)->valueDataFromYaml();
    }

    public function getItemValues(?callable $filter = null): Collection
    {
        return $this->getItems($filter)->reduce(function (Collection $c, ConfigurationItem $item, string $key) {
            $c->put($key, $item->value());
            return $c;
        }, new Collection());
    }

    public function getItems(?callable $filter = null): Collection
    {
        $items = $this->configurationResolver->getItems($this->application, $this->category);

        if (null === $filter) {
            return $items;
        }

        return $items->filter($filter);
    }

    public function implodeItemValues(string $source, string $delimiter = '', array $replacements = []): string
    {
        $sourceList = $this->getItemValueFromYaml($source);

        return implode($delimiter, array_reduce($sourceList, function (array $c, string $key) use ($replacements) {
            $c[] = $this->getItem($key)->value($replacements);
            return $c;
        }, []));
    }

    public function mergeItemData(string $source): array
    {
        $sourceList = $this->getItemValueFromYaml($source);

        return array_merge(...array_reduce($sourceList, function (array $c, string $key) {
            $c[] = $this->getItemValueFromYaml($key);
            return $c;
        }, []));
    }
}
