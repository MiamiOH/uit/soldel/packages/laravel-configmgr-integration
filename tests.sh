set -e

VERSION=8.0
TESTBENCH=6.0
PHPUNIT=9.0

POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
  --illuminate)
    VERSION="$2"
    shift # past argument
    shift # past value
    ;;
  *)                   # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift              # past argument
    ;;
  esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

case $VERSION in
  # Requires running in PHP 7.3 container to resolve properly.
  "8.0")
    TESTBENCH="6.5"
    PHPUNIT="9.5"
    ;;
  # Requires running in PHP 8.1 or higher container to resolve properly.
  "9.0")
    TESTBENCH="7.4"
    PHPUNIT="9.5"
    ;;
  "10.0")
    TESTBENCH="8.0"
    PHPUNIT="10.1"
    ;;
  # Requires running in PHP 8.2 container to resolve properly.
  "11.0")
    TESTBENCH="9.0"
    PHPUNIT="11.0"
    ;;
esac

echo Running tests with illuminate $VERSION testbench $TESTBENCH phpunit $PHPUNIT

if [ -e vendor ]; then
  rm -r vendor
fi

if [ -e composer.lock ]; then
  rm composer.lock
fi

cp composer.json composer.json.bak

composer remove --dev --no-scripts --no-interaction orchestra/testbench

composer remove --dev --no-scripts --no-interaction phpunit/phpunit


# Removing when running tests because this causes issues resolving at not up to date
# illuminate/support and orchestra/test-bench combinations.
composer remove --dev --no-scripts --no-interaction roave/security-advisories

composer require  --no-scripts --no-interaction \
  illuminate/support:^$VERSION \
  -W

composer require --dev  --no-scripts --no-interaction \
  orchestra/testbench:^$TESTBENCH \
  -W

  composer require --dev  --no-scripts --no-interaction \
   phpunit/phpunit:^$PHPUNIT \
    -W


mv composer.json.bak composer.json



vendor/bin/phpunit tests
