<?php

namespace MiamiOH\ConfigManager\Exceptions;

class NotAuthorizedException extends ConfigurationException
{
}
